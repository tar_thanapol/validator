package th.co.digio.unit.test.validator.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages = {"th.co.digio.validator"})
@EntityScan(basePackages = {"th.co.digio.validator.entity"})
@EnableJpaRepositories(basePackages = "th.co.digio.validator.repository")
@SpringBootApplication
public class ValidatorTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ValidatorTestApplication.class, args);
	}

}
