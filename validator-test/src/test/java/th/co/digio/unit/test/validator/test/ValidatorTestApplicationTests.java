package th.co.digio.unit.test.validator.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.Assert;
import th.co.digio.validator.implement.ValidateServiceImpl;
import th.co.digio.validator.service.ProvinceService;
import th.co.digio.validator.service.ValidateService;

@SpringBootTest
class ValidatorTestApplicationTests {


	@Autowired
	ValidateService validateService;

	@Autowired
	ValidateServiceImpl validateServiceImpl;

	@Autowired
	ProvinceService provinceService;

	@Test
	void contextLoads() {
	}

	@Test
	void validationFirstName(){
		//System.out.println(pathDatafile);
		//En
		Assertions.assertEquals(true, validateService.validateFirstName("boonprakom").isStatus());
		Assertions.assertEquals(true, validateService.validateFirstName("BOONPRAKOM").isStatus());
		Assertions.assertEquals(true, validateService.validateFirstName("BoonpraKom").isStatus());

		Assertions.assertEquals(false, validateService.validateFirstName("boon!prakom!").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("BOON!PRAKOM!").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("Boon!praKom!").isStatus());

		Assertions.assertEquals(false, validateService.validateFirstName("0boonprakom9").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("0BOONPRAKOM9").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("0BoonpraKom9").isStatus());

		Assertions.assertEquals(false, validateService.validateFirstName("boonpra kom").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("BOONPRA KOM").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("Boonpra Kom").isStatus());

		Assertions.assertEquals(false, validateService.validateFirstName("boonprบุญประคท").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("BOONPRAบุยน").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("Boonpraนรนย").isStatus());

		Assertions.assertEquals(false, validateService.validateFirstName("").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("").isStatus());

		//Th
		Assertions.assertEquals(true, validateService.validateFirstName("จารตากานนท์").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("ณ หนองคาย").isStatus());
		Assertions.assertEquals(true, validateService.validateFirstName("เกสสรา").isStatus());

		Assertions.assertEquals(false, validateService.validateFirstName("จา0รตากานนท์").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("ณ 0 หนองคาย").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("เกส0สรา").isStatus());

		Assertions.assertEquals(false, validateService.validateFirstName("!จารตาก0านนท์").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("ณ !หนองค0าย").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("เ!กสส0รา").isStatus());

		Assertions.assertEquals(false, validateService.validateFirstName("จารตASDากานนท์").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("ณ หนdsdsองคาย").isStatus()); //***
		Assertions.assertEquals(false, validateService.validateFirstName("เกdsaสสรา").isStatus());

		Assertions.assertEquals(false, validateService.validateFirstName("").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("").isStatus());
		Assertions.assertEquals(false, validateService.validateFirstName("").isStatus());
	}

	@Test
	void validationLastName(){
		//En
		Assertions.assertEquals(true, validateService.validateLastName("boonprakom").isStatus());
		Assertions.assertEquals(true, validateService.validateLastName("BOONPRAKOM").isStatus());
		Assertions.assertEquals(true, validateService.validateLastName("BoonpraKom").isStatus());

		Assertions.assertEquals(false, validateService.validateLastName("boon!prakom!").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("BOON!PRAKOM!").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("Boon!praKom!").isStatus());

		Assertions.assertEquals(false, validateService.validateLastName("0boonprakom9").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("0BOONPRAKOM9").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("0BoonpraKom9").isStatus());

		Assertions.assertEquals(true, validateService.validateLastName("boonpra kom").isStatus());
		Assertions.assertEquals(true, validateService.validateLastName("BOONPRA KOM").isStatus());
		Assertions.assertEquals(true, validateService.validateLastName("Boonpra Kom").isStatus());

		Assertions.assertEquals(false, validateService.validateLastName("boonprบุญประคท").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("BOONPRAบุยน").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("Boonpraนรนย").isStatus());

		Assertions.assertEquals(false, validateService.validateLastName("").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("").isStatus());

		//Th
		Assertions.assertEquals(true, validateService.validateLastName("จารตากานนท์").isStatus());
		Assertions.assertEquals(true, validateService.validateLastName("ณ หนองคาย").isStatus());
		Assertions.assertEquals(true, validateService.validateLastName("เกสสรา").isStatus());

		Assertions.assertEquals(false, validateService.validateLastName("จา0รตากานนท์").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("ณ 0 หนองคาย").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("เกส0สรา").isStatus());

		Assertions.assertEquals(false, validateService.validateLastName("!จารตาก0านนท์").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("ณ !หนองค0าย").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("เ!กสส0รา").isStatus());

		Assertions.assertEquals(false, validateService.validateLastName("จารตASDากานนท์").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("ณ หนdsdsองคาย").isStatus()); //***
		Assertions.assertEquals(false, validateService.validateLastName("เกdsaสสรา").isStatus());

		Assertions.assertEquals(false, validateService.validateLastName("").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("").isStatus());
		Assertions.assertEquals(false, validateService.validateLastName("").isStatus());
	}

	@Test
	void validationMobileNumber(){
		Assertions.assertEquals(false, validateService.validateMobileNumber("0612 45678").isStatus());
		Assertions.assertEquals(true, validateService.validateMobileNumber("0612045678").isStatus());
		Assertions.assertEquals(true, validateService.validateMobileNumber("0833692540").isStatus());
		Assertions.assertEquals(true, validateService.validateMobileNumber("0933692540").isStatus());

		Assertions.assertEquals(false, validateService.validateMobileNumber("093369254").isStatus());
		Assertions.assertEquals(false, validateService.validateMobileNumber("09336925401").isStatus());
		Assertions.assertEquals(false, validateService.validateMobileNumber("1933692540").isStatus());
		Assertions.assertEquals(false, validateService.validateMobileNumber("0433692540").isStatus());
		Assertions.assertEquals(false, validateService.validateMobileNumber("093369254!").isStatus());
		Assertions.assertEquals(false, validateService.validateMobileNumber("0933g92540").isStatus());

		Assertions.assertEquals(false, validateService.validateMobileNumber("0612 45678").isStatus());
		Assertions.assertEquals(false, validateService.validateMobileNumber("0212345678").isStatus());

	}

	@Test
	void validationPhoneNumber(){
		Assertions.assertEquals(false, validateService.validatePhoneNumber("0612345678").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneNumber("0833692540").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneNumber("0933692540").isStatus());
		Assertions.assertEquals(true, validateService.validatePhoneNumber("0433692540").isStatus());
		Assertions.assertEquals(true, validateService.validatePhoneNumber("023369254").isStatus());

		Assertions.assertEquals(false, validateService.validatePhoneNumber("09336925").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneNumber("09336925401").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneNumber("1933692540").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneNumber("193369254").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneNumber("093369254!").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneNumber("0933g925!").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneNumber("0433g9250").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneNumber("0212 45678").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneNumber("0612245678").isStatus());
	}

	@Test
	void validationProvince(){
		Assertions.assertEquals(false, provinceService.validateProvince("").isStatus());
		Assertions.assertEquals(true, provinceService.validateProvince("สิงห์บุรี").isStatus());
		Assertions.assertEquals(false, provinceService.validateProvince("สิงห์บุรี!").isStatus());
		Assertions.assertEquals(false, provinceService.validateProvince("สิงห์บุรี0").isStatus());
		Assertions.assertEquals(false, provinceService.validateProvince("สิงห์0บุรี!").isStatus());
		Assertions.assertEquals(false, provinceService.validateProvince("สิงห์รี").isStatus());
		Assertions.assertEquals(false, provinceService.validateProvince("สิงasdaห์รี").isStatus());

		Assertions.assertEquals(true, provinceService.validateProvince("Sing Buri").isStatus());
		Assertions.assertEquals(false, provinceService.validateProvince("Sing Buri!").isStatus());
		Assertions.assertEquals(false, provinceService.validateProvince("Sing Buri0").isStatus());
		Assertions.assertEquals(false, provinceService.validateProvince("Sing0 Buri!").isStatus());
		Assertions.assertEquals(false, provinceService.validateProvince("Sing Bu").isStatus());
		Assertions.assertEquals(false, provinceService.validateProvince("Sing Buรี").isStatus());
		Assertions.assertEquals(false, provinceService.validateProvince("SingBuri").isStatus());
	}

	@Test
	void validationDistrict(){
		Assertions.assertEquals(false, validateService.validateDistrict("").isStatus());
		Assertions.assertEquals(true, validateService.validateDistrict("ศรีวิไล").isStatus());
		Assertions.assertEquals(false, validateService.validateDistrict("ศรีวิไล!").isStatus());
		Assertions.assertEquals(false, validateService.validateDistrict("ศรีวิไล0").isStatus());
		Assertions.assertEquals(false, validateService.validateDistrict("ศรีวิไ").isStatus());
		Assertions.assertEquals(false, validateService.validateDistrict("ศรีวิไsdasd").isStatus());

		Assertions.assertEquals(true, validateService.validateDistrict("Khet Din Daeng").isStatus());
		Assertions.assertEquals(false, validateService.validateDistrict("Khet Din Daeng!").isStatus());
		Assertions.assertEquals(false, validateService.validateDistrict("Khet0 Din Daeng").isStatus());
		Assertions.assertEquals(false, validateService.validateDistrict("Khet0 Din Daeng!").isStatus());
		Assertions.assertEquals(false, validateService.validateDistrict("Khet Din Daengฟหก").isStatus());
		Assertions.assertEquals(false, validateService.validateDistrict("KhetDinDaeng").isStatus());
	}

	@Test
	void validationFullName(){
		//En
		Assertions.assertEquals(true, validateService.validateFullName("thanapol boonprakom").isStatus());
		Assertions.assertEquals(true, validateService.validateFullName("THANAPOL BOONPRAKOM").isStatus());
		Assertions.assertEquals(true, validateService.validateFullName("Thanapol BoonpraKom").isStatus());

		Assertions.assertEquals(false, validateService.validateFullName("thanapol boon!prakom!").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("THANAPO! BOON!PRAKOM!").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("Thanpo! Boon!praKom!").isStatus());

		Assertions.assertEquals(false, validateService.validateFullName("1thanapol 0boonprakom9").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("1HANAPOL  0BOONPRAKOM9").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("THANAPO3 0BoonpraKom9").isStatus());

		Assertions.assertEquals(true, validateService.validateFullName("thanapol boonpra kom").isStatus());
		Assertions.assertEquals(true, validateService.validateFullName("THANAPOL BOONPRA KOM").isStatus());
		Assertions.assertEquals(true, validateService.validateFullName("Thanapol Boonpra Kom").isStatus());

		Assertions.assertEquals(false, validateService.validateFullName("thanaพล boonprบุญประคท").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("thanaพล BOONPRAบุยน").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("thanaพล Boonpra นรนย").isStatus());

		Assertions.assertEquals(false, validateService.validateFullName("").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("").isStatus());

		//Th
		Assertions.assertEquals(true, validateService.validateFullName("บารมี จารตากานนท์").isStatus());
		Assertions.assertEquals(true, validateService.validateFullName("ธนพล ณ หนองคาย").isStatus());
		Assertions.assertEquals(true, validateService.validateFullName("เกสสรา จำปา").isStatus());

		Assertions.assertEquals(false, validateService.validateFullName("บารมี จา0รตากานนท์").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("2ธนพล ณ 0 หนองคาย").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("เกส0สรา จำปา").isStatus());

		Assertions.assertEquals(false, validateService.validateFullName("บารมี !จารตาก0านนท์").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("ธนพล ณ !หนองค0าย").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("เ!กสส0รา จำปา").isStatus());

		Assertions.assertEquals(false, validateService.validateFullName("baramee จารตASDากานนท์").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("thanapol ณ หนdsdsองคาย").isStatus()); //***
		Assertions.assertEquals(false, validateService.validateFullName("เกdsaสสรา jampa").isStatus());

		Assertions.assertEquals(false, validateService.validateFullName("").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("").isStatus());
		Assertions.assertEquals(false, validateService.validateFullName("").isStatus());
	}

	@Test
	void validationPhoneAndMobile(){
		Assertions.assertEquals(true, validateService.validatePhoneAndMobileNumber("0612345678").isStatus());
		Assertions.assertEquals(true, validateService.validatePhoneAndMobileNumber("0833692540").isStatus());
		Assertions.assertEquals(true, validateService.validatePhoneAndMobileNumber("0933692540").isStatus());
		Assertions.assertEquals(true, validateService.validatePhoneAndMobileNumber("0433692540").isStatus());
		Assertions.assertEquals(true, validateService.validatePhoneAndMobileNumber("023369254").isStatus());

		Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("09336925").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("09336925401").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("1933692540").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("193369254").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("093369254!").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("0933g925!").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("0433g9250").isStatus());
		Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("0212 45678").isStatus());
	}
}
