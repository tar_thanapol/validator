package th.co.digio.validator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import th.co.digio.validator.implement.ProvinceServiceImpl;
import th.co.digio.validator.DTO.ResponseResultDTO;

import java.util.logging.Logger;

@RestController
@CrossOrigin("*")
public class ProvinceController {

    private static final Logger logger = Logger.getLogger(String.valueOf(ProvinceController.class));

    @Autowired
    private ProvinceServiceImpl provinceService;

    @RequestMapping(value = "checkProvince", produces = {"application/json"}, method = RequestMethod.POST)
    public ResponseResultDTO checkMobileNumber(@RequestBody String text) {
        logger.info("validate provice : " + text);
        ResponseResultDTO res = new ResponseResultDTO();
        res = provinceService.validateProvince(text);
        return res;
    }
}
