package th.co.digio.validator.service;

import org.springframework.stereotype.Service;
import th.co.digio.validator.DTO.ResponseResultDTO;

@Service
public interface ProvinceService {

    ResponseResultDTO validateProvince(String text);
}
