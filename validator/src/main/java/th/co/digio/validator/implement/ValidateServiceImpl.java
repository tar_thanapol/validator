package th.co.digio.validator.implement;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import th.co.digio.validator.DTO.AmphureDTO;
import th.co.digio.validator.DTO.ResponseResultDTO;
import th.co.digio.validator.enummuration.CommonValidatorStatus;
import th.co.digio.validator.service.ValidateService;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Component
public class ValidateServiceImpl implements ValidateService {

    @Value("${validate.distric.location}")
    String pathDataFileDistrict;

    private static final Logger logger = Logger.getLogger(String.valueOf(ValidateServiceImpl.class));

    @Override
    public ResponseResultDTO validateFirstName(String name) {
        logger.info("Validate : " + name);
        name = name.trim();
        ResponseResultDTO response = new ResponseResultDTO();
        boolean valid;
        if (name.isEmpty()) {
            response.setCode(CommonValidatorStatus.INVALID_EMPTY_TEXT.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_EMPTY_TEXT.getMessageEn());
            response.setStatus(false);
            return response;
        }
        if (name.matches("[a-zA-Z]*")) {
            valid = name.matches("[a-zA-Z]*");
            logger.info("Validate En : " + valid);
        } else if (name.matches("[ก-๐]*")) {
            valid = name.matches("[ก-๐]*");
            logger.info("Validate Th : " + valid);
        } else if (name.matches("[a-zA-Zก-๐]*")) {
            valid = name.matches("[a-zA-Zก-๐]*");
            logger.info("Validate Th En : " + valid);
            response.setMessage(CommonValidatorStatus.INVALID_NAME_NOT_ONLY_ONE.getMessageEn());
            response.setStatus(false);
            return response;
        } else {
            char[] nameArr = name.toCharArray();
            String strError = "";
            boolean th = false, en = false;
            for (int i = 0; i < nameArr.length; i++) {
                if (!String.valueOf(nameArr[i]).matches("[a-zA-Zก-๐]*")) {
                    logger.warning("char Digit " + i + " : " + nameArr[i]);
                    if (nameArr[i] == ' ') {
                        strError = strError + "space" + " ";
                        continue;
                    }
                    strError = strError + nameArr[i] + " ";
                } else {
                    if (String.valueOf(nameArr[i]).matches("[a-zA-Z]*")) {
                        en = true;
                    } else if (String.valueOf(nameArr[i]).matches("[ก-๐]*")) {
                        th = true;
                    }
                }
            }
            if (!strError.isEmpty()) {
                response.setCode(CommonValidatorStatus.INVALID_CHARACTER.getCode());
                response.setMessage(CommonValidatorStatus.INVALID_CHARACTER.getMessageEn() + " " + strError);
                response.setStatus(false);
                return response;
            }
            if (en == true && th == true) {
                response.setCode(CommonValidatorStatus.INVALID_NAME_NOT_ONLY_ONE.getCode());
                response.setMessage(CommonValidatorStatus.INVALID_NAME_NOT_ONLY_ONE.getMessageEn());
                response.setStatus(false);
                return response;
            }
        }
        response.setCode(CommonValidatorStatus.SUCCESS.getCode());
        response.setMessage(CommonValidatorStatus.SUCCESS.getMessageEn());
        response.setStatus(true);
        return response;
    }

    @Override
    public ResponseResultDTO validateLastName(String lastname) {
        logger.info("Validate : " + lastname);
        lastname = lastname.trim();
        ResponseResultDTO response = new ResponseResultDTO();
        boolean valid;
        if (lastname.isEmpty()) {
            response.setCode(CommonValidatorStatus.INVALID_EMPTY_TEXT.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_EMPTY_TEXT.getMessageEn());
            response.setStatus(false);
            return response;
        }
        if (lastname.matches("[a-zA-Z]*")) {
            valid = lastname.matches("[a-zA-Z]*");
            logger.info("Validate En : " + valid);
        } else if (lastname.matches("[ก-๐]*")) {
            valid = lastname.matches("[ก-๐]*");
            logger.info("Validate Th : " + valid);
        } else if (lastname.matches("[a-zA-Zก-๐]*")) {
            valid = lastname.matches("[a-zA-Zก-๐]*");
            logger.info("Validate Th En : " + valid);
            response.setCode(CommonValidatorStatus.INVALID_NAME_NOT_ONLY_ONE.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_NAME_NOT_ONLY_ONE.getMessageEn());
            response.setStatus(false);
            return response;
        } else {
            char[] nameArr = lastname.toCharArray();
            String strError = "";
            boolean th = false, en = false;
            for (int i = 0; i < nameArr.length; i++) {
                if (!String.valueOf(nameArr[i]).matches("[a-zA-Zก-๐]*")) {
                    logger.warning("char Digit " + i + " : " + nameArr[i]);
                    if (nameArr[i] == ' ') {
                        continue;
                    }
                    strError = strError + nameArr[i] + " ";
                } else {
                    if (String.valueOf(nameArr[i]).matches("[a-zA-Z]*")) {
                        en = true;
                    } else if (String.valueOf(nameArr[i]).matches("[ก-๐]*")) {
                        th = true;
                    }
                }
            }

            if (!strError.isEmpty()) {
                response.setCode(CommonValidatorStatus.INVALID_CHARACTER.getCode());
                response.setMessage(CommonValidatorStatus.INVALID_CHARACTER.getMessageEn() + " " + strError);
                response.setStatus(false);
                return response;
            }
            if (en == true && th == true) {
                response.setCode(CommonValidatorStatus.INVALID_LASTNAME_NOT_ONLY_ONE.getCode());
                response.setMessage(CommonValidatorStatus.INVALID_LASTNAME_NOT_ONLY_ONE.getMessageEn());
                response.setStatus(false);
                return response;
            }
        }
        response.setCode(CommonValidatorStatus.SUCCESS.getCode());
        response.setMessage(CommonValidatorStatus.SUCCESS.getMessageEn());
        response.setStatus(true);
        return response;
    }

    @Override
    public ResponseResultDTO validateMobileNumber(String number) {
        logger.info("Validate mobile : " + number);
        number = number.trim();
        ResponseResultDTO response = new ResponseResultDTO();
        if (number.length() != 10) {
            response.setCode(CommonValidatorStatus.INVALID_LENGTH_NUMBER.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_LENGTH_NUMBER.getMessageEn() + " is " + number.length());
            response.setStatus(false);
            return response;
        }
        if (number.matches("^0")) {
            response.setCode(CommonValidatorStatus.INVALID_START_ZERO.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_START_ZERO.getMessageEn());
            response.setStatus(false);
            return response;
        }
        if (!number.matches("^0[689][0-9]{8}")) {
            char[] nameArr = number.toCharArray();
            String subStr = number.substring(0, 2);
            String strError = "";
            if (!subStr.matches("^0[689]")) {
                response.setCode(CommonValidatorStatus.INVALID_NOT_MOBILE_NUMBER.getCode());
                response.setMessage(CommonValidatorStatus.INVALID_NOT_MOBILE_NUMBER.getMessageEn());
                response.setStatus(false);
                return response;
            } else {
                for (int i = 0; i < nameArr.length; i++) {
                    if (!String.valueOf(nameArr[i]).matches("[0-9]")) {
                        logger.warning("char Digit " + i + " : " + nameArr[i]);
                        if (nameArr[i] == ' ') {
                            strError = strError + "space" + " ";
                            continue;
                        }
                        strError = strError + nameArr[i] + " ";
                    }
                }
                if (!strError.isEmpty()) {
                    response.setCode(CommonValidatorStatus.INVALID_NUMBER_CHARACTER.getCode());
                    response.setMessage(CommonValidatorStatus.INVALID_NUMBER_CHARACTER.getMessageEn() + " " + strError);
                    response.setStatus(false);
                    return response;
                }
            }
        }
        response.setCode(CommonValidatorStatus.SUCCESS.getCode());
        response.setMessage(CommonValidatorStatus.SUCCESS.getMessageEn());
        response.setStatus(true);
        return response;
    }

    @Override
    public ResponseResultDTO validatePhoneNumber(String number) {
        logger.info("Validate Phone : " + number);
        number = number.trim();
        ResponseResultDTO response = new ResponseResultDTO();
        if (number.length() > 10 || number.length() < 9) {
            response.setCode(CommonValidatorStatus.INVALID_LENGTH_NUMBER.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_LENGTH_NUMBER.getMessageEn() + " is " + number.length());
            response.setStatus(false);
            return response;
        }
        if (number.matches("^0")) {
            response.setCode(CommonValidatorStatus.INVALID_START_ZERO.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_START_ZERO.getMessageEn());
            response.setStatus(false);
            return response;
        }
        if (!number.matches("^0[23457][0-9]{7,8}")) {
            char[] nameArr = number.toCharArray();
            String subStr = number.substring(0, 2);
            String strError = "";
            for (int i = 0; i < nameArr.length; i++) {
                if (!String.valueOf(nameArr[i]).matches("[0-9]")) {
                    logger.warning("char Digit " + i + " : " + nameArr[i]);
                    if (nameArr[i] == ' ') {
                        strError = strError + "space" + " ";
                        continue;
                    }
                    strError = strError + nameArr[i] + " ";
                }
            }
            if (!strError.isEmpty()) {
                response.setCode(CommonValidatorStatus.INVALID_NUMBER_CHARACTER.getCode());
                response.setMessage(CommonValidatorStatus.INVALID_NUMBER_CHARACTER.getMessageEn() + " " + strError);
                response.setStatus(false);
                return response;
            }
            response.setCode(CommonValidatorStatus.INVALID_NOT_PHONE_NUMBER.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_NOT_PHONE_NUMBER.getMessageEn());
            response.setStatus(false);
            return response;
        }
        response.setCode(CommonValidatorStatus.SUCCESS.getCode());
        response.setMessage(CommonValidatorStatus.SUCCESS.getMessageEn());
        response.setStatus(true);
        return response;
    }

    @Override
    public ResponseResultDTO validateDistrict(String text) {
        ResponseResultDTO response = new ResponseResultDTO();
        if (text.isEmpty()) {
            response.setCode(CommonValidatorStatus.INVALID_EMPTY_TEXT.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_EMPTY_TEXT.getMessageEn());
            response.setStatus(false);
            return response;
        }
        if (text.matches("[a-zA-Z]*")) {
            logger.info("Validate En  ");
        } else if (text.matches("[ก-๐]*")) {
            logger.info("Validate Th : ");
        } else if (text.matches("[a-zA-Zก-๐]*")) {
            logger.info("Validate Th En : ");
            response.setCode(CommonValidatorStatus.INVALID_AMPHURE_NOT_ONLY_ONE.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_AMPHURE_NOT_ONLY_ONE.getMessageEn());
            response.setStatus(false);
            return response;
        } else {
            char[] nameArr = text.toCharArray();
            String strError = "";
            boolean th = false, en = false;
            for (int i = 0; i < nameArr.length; i++) {
                if (!String.valueOf(nameArr[i]).matches("[a-zA-Zก-๐]*")) {
                    logger.warning("char Digit " + i + " : " + nameArr[i]);
                    if (nameArr[i] == ' ') {
                        continue;
                    }
                    strError = strError + nameArr[i] + " ";
                } else {
                    if (String.valueOf(nameArr[i]).matches("[a-zA-Z]*")) {
                        en = true;
                    } else if (String.valueOf(nameArr[i]).matches("[ก-๐]*")) {
                        th = true;
                    }
                }
            }
            if (!strError.isEmpty()) {
                response.setCode(CommonValidatorStatus.INVALID_CHARACTER.getCode());
                response.setMessage(CommonValidatorStatus.INVALID_CHARACTER.getMessageEn() + " " + strError);
                response.setStatus(false);
                return response;
            }
            if (en == true && th == true) {
                response.setCode(CommonValidatorStatus.INVALID_LASTNAME_NOT_ONLY_ONE.getCode());
                response.setMessage(CommonValidatorStatus.INVALID_LASTNAME_NOT_ONLY_ONE.getMessageEn());
                response.setStatus(false);
                return response;
            }
        }
        List<AmphureDTO> amphureDTOS = new ArrayList<>();
        try {
            String expected_value = "Hello, world!";
//            String file = "src/main/resources/testfile.txt";

            BufferedReader reader = new BufferedReader(new FileReader(pathDataFileDistrict));
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                String[] listStr = currentLine.split("\\|");
                AmphureDTO amphureDTO = new AmphureDTO();
                amphureDTO.setCode(listStr[1].trim());
                amphureDTO.setNameTh(listStr[2].trim());
                amphureDTO.setNameEn(listStr[3].trim());
                amphureDTOS.add(amphureDTO);
            }
            reader.close();
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        boolean findCheck = false;
        for (AmphureDTO amphureDTO : amphureDTOS) {
            if (text.equalsIgnoreCase(amphureDTO.getNameEn()) || text.equalsIgnoreCase(amphureDTO.getNameTh())) {
                findCheck = true;
                break;
            }
        }
        if (findCheck == false) {
            response.setCode(CommonValidatorStatus.INVALID_AMPHURE.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_AMPHURE.getMessageEn());
            response.setStatus(false);
            return response;
        }
        response.setCode(CommonValidatorStatus.SUCCESS.getCode());
        response.setMessage(CommonValidatorStatus.SUCCESS.getMessageEn());
        response.setStatus(true);
        return response;
    }

    @Override
    public ResponseResultDTO validateFullName(String text) {
        logger.info("Validate : " + text);
        text = text.trim();
        ResponseResultDTO response = new ResponseResultDTO();
        boolean valid;
        if (text.isEmpty()) {
            response.setCode(CommonValidatorStatus.INVALID_EMPTY_TEXT.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_EMPTY_TEXT.getMessageEn());
            response.setStatus(false);
            return response;
        }
        char[] nameArr = text.toCharArray();
        String strError = "";
        boolean th = false, en = false;
        for (int i = 0; i < nameArr.length; i++) {
            if (!String.valueOf(nameArr[i]).matches("[a-zA-Zก-๐]*")) {
                logger.warning("char Digit " + i + " : " + nameArr[i]);
                if (nameArr[i] == ' ') {
                    continue;
                }
                strError = strError + nameArr[i] + " ";
            } else {
                if (String.valueOf(nameArr[i]).matches("[a-zA-Z]*")) {
                    en = true;
                } else if (String.valueOf(nameArr[i]).matches("[ก-๐]*")) {
                    th = true;
                }
            }
        }

        if (!strError.isEmpty()) {
            response.setCode(CommonValidatorStatus.INVALID_CHARACTER.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_CHARACTER.getMessageEn() + " " + strError);
            response.setStatus(false);
            return response;
        }
        if (en == true && th == true) {
            response.setCode(CommonValidatorStatus.INVALID_LASTNAME_NOT_ONLY_ONE.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_LASTNAME_NOT_ONLY_ONE.getMessageEn());
            response.setStatus(false);
            return response;
        }

        response.setCode(CommonValidatorStatus.SUCCESS.getCode());
        response.setMessage(CommonValidatorStatus.SUCCESS.getMessageEn());
        response.setStatus(true);
        return response;
    }

    @Override
    public ResponseResultDTO validatePhoneAndMobileNumber(String number) {
        logger.info("Validate Phone and mobile : " + number);
        number = number.trim();
        ResponseResultDTO response = new ResponseResultDTO();
        if (number.length() > 10 || number.length() < 9) {
            response.setCode(CommonValidatorStatus.INVALID_LENGTH_NUMBER.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_LENGTH_NUMBER.getMessageEn() + " is " + number.length());
            response.setStatus(false);
            return response;
        }
        if (number.matches("^0")) {
            response.setCode(CommonValidatorStatus.INVALID_START_ZERO.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_START_ZERO.getMessageEn());
            response.setStatus(false);
            return response;
        }
        if (number.length() <= 10 && number.length() >= 9) {
            if (number.matches("^0[23457][0-9]{7,8}")) {
                logger.info("Is phone number : " + number);
            }
            else if (number.matches("^0[689][0-9]{8}")) {
                logger.info("Is mobile number : " + number);
            }else{
                char[] nameArr = number.toCharArray();
                String strError = "";
                for (int i = 0; i < nameArr.length; i++) {
                    if (!String.valueOf(nameArr[i]).matches("[0-9]")) {
                        logger.warning("char Digit " + i + " : " + nameArr[i]);
                        if (nameArr[i] == ' ') {
                            strError = strError + "space" + " ";
                            continue;
                        }
                        strError = strError + nameArr[i] + " ";
                    }
                }
                if (!strError.isEmpty()) {
                    response.setCode(CommonValidatorStatus.INVALID_NUMBER_CHARACTER.getCode());
                    response.setMessage(CommonValidatorStatus.INVALID_NUMBER_CHARACTER.getMessageEn() + " " + strError);
                    response.setStatus(false);
                    return response;
                }
                response.setCode(CommonValidatorStatus.INVALID_NUMBER.getCode());
                response.setMessage(CommonValidatorStatus.INVALID_NUMBER.getMessageEn());
                response.setStatus(false);
                return response;
            }
        }
        response.setCode(CommonValidatorStatus.SUCCESS.getCode());
        response.setMessage(CommonValidatorStatus.SUCCESS.getMessageEn());
        response.setStatus(true);
        return response;
    }
}
