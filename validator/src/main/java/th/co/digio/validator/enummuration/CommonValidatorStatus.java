package th.co.digio.validator.enummuration;

public enum CommonValidatorStatus {

    SUCCESS("0000", "SUCCESS"),
    INVALID_CHARACTER("1000", "Invalid Character"),
    INVALID_EMPTY_TEXT("1001", "Invalid empty text"),
    INVALID_NAME_NOT_ONLY_ONE("1002", "Invalid name only chose one language"),
    INVALID_LASTNAME_NOT_ONLY_ONE("1002", "Invalid lastname only chose one language"),

    INVALID_LENGTH_NUMBER("1010", "Invalid length phone number"),
    INVALID_START_ZERO("1011", "Invalid phone number can start with zero"),
    INVALID_NUMBER_CHARACTER("1012", "Invalid number character"),
    INVALID_NUMBER("1012", "Invalid number"),
    INVALID_NOT_MOBILE_NUMBER("1013", "Invalid start number can start only 08 06 09"),
    INVALID_NOT_PHONE_NUMBER("1014", "Invalid start number can start only 02 03 04 05 07"),

    INVALID_PROVINCE("2000", "Invalid province"),
    INVALID_PROVINCE_NOT_ONLY_ONE("2001", "Invalid province only chose one language"),

    INVALID_AMPHURE("3000", "Invalid amphure"),
    INVALID_AMPHURE_NOT_ONLY_ONE("2001", "Invalid amphure only chose one language"),
    ;

    String code;
    String messageEn;

    CommonValidatorStatus(String code, String messageEn){
        this.code = code;
        this.messageEn = messageEn;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessageEn() {
        return messageEn;
    }

    public void setMessageEn(String messageEn) {
        this.messageEn = messageEn;
    }

}
