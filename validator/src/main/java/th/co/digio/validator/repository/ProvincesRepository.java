package th.co.digio.validator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.digio.validator.entity.Provinces;

import java.util.List;

@Repository
public interface ProvincesRepository extends JpaRepository<Provinces, Long> {

    Provinces findByNameEnLikeOrNameThLike(String nameEn, String nameTh);
    List<Provinces> findByNameEnContainingIgnoreCaseOrNameThContainingIgnoreCase(String nameEn, String nameTh);
    List<Provinces> findByNameEnOrNameTh(String nameEn, String nameTh);
}
