package th.co.digio.validator.service;

import org.springframework.stereotype.Service;
import th.co.digio.validator.DTO.ResponseResultDTO;

@Service
public interface ValidateService {

    ResponseResultDTO validateFirstName(String name);

    ResponseResultDTO validateLastName(String lastname);

    ResponseResultDTO validatePhoneNumber(String number);

    ResponseResultDTO validateMobileNumber(String number);

    ResponseResultDTO validateDistrict(String text);

    ResponseResultDTO validateFullName(String text);

    ResponseResultDTO validatePhoneAndMobileNumber(String text);
}
