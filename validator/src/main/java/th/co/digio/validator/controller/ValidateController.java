package th.co.digio.validator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import th.co.digio.validator.DTO.ResponseResultDTO;
import th.co.digio.validator.service.ValidateService;

import java.util.logging.Logger;

@RestController
@CrossOrigin("*")
public class ValidateController {
    private static final Logger logger = Logger.getLogger(String.valueOf(ValidateController.class));

    @Autowired
    private ValidateService validateService;

    @RequestMapping(value = "testController", produces = {"application/json"}, method = RequestMethod.GET)
    public String testMethod() {

        return "Hello World";
    }

    @RequestMapping(value = "checkFirstName", produces = {"application/json"}, method = RequestMethod.POST)
    public ResponseResultDTO checkFirstName(@RequestBody String text) {
        logger.info("Is : " + text);
        ResponseResultDTO res = new ResponseResultDTO();
        res = validateService.validateFirstName(text);
        return res;
    }

    @RequestMapping(value = "checkLastName", produces = {"application/json"}, method = RequestMethod.POST)
    public ResponseResultDTO checkLastName(@RequestBody String text) {
        logger.info("Is : " + text);
        ResponseResultDTO res = new ResponseResultDTO();
        res = validateService.validateLastName(text);
        return res;
    }

    @RequestMapping(value = "checkPhoneNumber", produces = {"application/json"}, method = RequestMethod.POST)
    public ResponseResultDTO checkPhoneNumber(@RequestBody String text) {
        logger.info("Is : " + text);
        ResponseResultDTO res = new ResponseResultDTO();
        res = validateService.validatePhoneNumber(text);
        return res;
    }

    @RequestMapping(value = "checkMobileNumber", produces = {"application/json"}, method = RequestMethod.POST)
    public ResponseResultDTO checkMobileNumber(@RequestBody String text) {
        logger.info("Is : " + text);
        ResponseResultDTO res = new ResponseResultDTO();
        res = validateService.validateMobileNumber(text);
        return res;
    }

    @RequestMapping(value = "checkAmphures", produces = {"application/json"}, method = RequestMethod.POST)
    public ResponseResultDTO checkAmphures(@RequestBody String text) {
        logger.info("Is : " + text);
        ResponseResultDTO res = new ResponseResultDTO();
        res = validateService.validateDistrict(text);
        return res;
    }
}
