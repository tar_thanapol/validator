package th.co.digio.validator.implement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.co.digio.validator.enummuration.CommonValidatorStatus;
import th.co.digio.validator.DTO.ResponseResultDTO;
import th.co.digio.validator.entity.Provinces;
import th.co.digio.validator.repository.ProvincesRepository;
import th.co.digio.validator.service.ProvinceService;

import java.util.List;
import java.util.logging.Logger;

@Component
public class ProvinceServiceImpl implements ProvinceService {

    private static final Logger logger = Logger.getLogger(String.valueOf(ProvinceServiceImpl.class));

    @Autowired
    private ProvincesRepository provincesRepository;

    @Override
    public ResponseResultDTO validateProvince(String text) {
        ResponseResultDTO response = new ResponseResultDTO();
        boolean valid;
        text = text.trim();
        if (text.isEmpty()) {
            response.setCode(CommonValidatorStatus.INVALID_EMPTY_TEXT.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_EMPTY_TEXT.getMessageEn());
            response.setStatus(false);
            return response;
        }
        if (text.matches("[a-zA-Z]*")) {
            valid = text.matches("[a-zA-Z]*");
            logger.info("Validate En : " + valid);
        } else if (text.matches("[ก-๐]*")) {
            valid = text.matches("[ก-๐]*");
            logger.info("Validate Th : " + valid);
        } else if (text.matches("[a-zA-Zก-๐]*")) {
            valid = text.matches("[a-zA-Zก-๐]*");
            logger.info("Validate Th En : " + valid);
            response.setCode(CommonValidatorStatus.INVALID_PROVINCE_NOT_ONLY_ONE.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_PROVINCE_NOT_ONLY_ONE.getMessageEn());
            response.setStatus(false);
            return response;
        } else {
            char[] nameArr = text.toCharArray();
            String strError = "";
            boolean th = false, en = false;
            for (int i = 0; i < nameArr.length; i++) {
                if (!String.valueOf(nameArr[i]).matches("[a-zA-Zก-๐]*")) {
                    logger.warning("char Digit " + i + " : " + nameArr[i]);
                    if (nameArr[i] == ' ') {
                        continue;
                    }
                    strError = strError + nameArr[i] + " ";
                } else {
                    if (String.valueOf(nameArr[i]).matches("[a-zA-Z]*")) {
                        en = true;
                    } else if (String.valueOf(nameArr[i]).matches("[ก-๐]*")) {
                        th = true;
                    }
                }
            }
            if (!strError.isEmpty()) {
                response.setCode(CommonValidatorStatus.INVALID_CHARACTER.getCode());
                response.setMessage(CommonValidatorStatus.INVALID_CHARACTER.getMessageEn() + " " + strError);
                response.setStatus(false);
                return response;
            }
            if (en == true && th == true) {
                response.setCode(CommonValidatorStatus.INVALID_LASTNAME_NOT_ONLY_ONE.getCode());
                response.setMessage(CommonValidatorStatus.INVALID_LASTNAME_NOT_ONLY_ONE.getMessageEn());
                response.setStatus(false);
                return response;
            }
        }
        List<Provinces> provinces = provincesRepository.findByNameEnOrNameTh(text, text);
        if (provinces == null || provinces.size() == 0) {
            logger.info("province null");
            response.setCode(CommonValidatorStatus.INVALID_PROVINCE.getCode());
            response.setMessage(CommonValidatorStatus.INVALID_PROVINCE.getMessageEn());
            response.setStatus(false);
            return response;
        }
        logger.info("province is " + provinces);
        response.setCode(CommonValidatorStatus.SUCCESS.getCode());
        response.setMessage(CommonValidatorStatus.SUCCESS.getMessageEn());
        response.setStatus(true);
        return response;
    }
}
