package th.co.digio.validator.implement;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;
import th.co.digio.validator.DTO.ResponseResultDTO;
import th.co.digio.validator.service.ProvinceService;
import th.co.digio.validator.service.ValidateService;


@SpringBootTest
class ValidateServiceImplTest {


    @Autowired
    ValidateService validateService;

    @Autowired
    ProvinceService provinceService;

    @Test
    void validateFirstName() {

        Assertions.assertEquals(false, provinceService.validateProvince("").isStatus());
        Assertions.assertEquals(true, provinceService.validateProvince("สิงห์บุรี").isStatus());
        Assertions.assertEquals(false, provinceService.validateProvince("สิงห์บุรี!").isStatus());
        Assertions.assertEquals(false, provinceService.validateProvince("สิงห์บุรี0").isStatus());
        Assertions.assertEquals(false, provinceService.validateProvince("สิงห์0บุรี!").isStatus());
        Assertions.assertEquals(false, provinceService.validateProvince("สิงห์รี").isStatus());
        Assertions.assertEquals(false, provinceService.validateProvince("สิงasdaห์รี").isStatus());

        Assertions.assertEquals(true, provinceService.validateProvince("Sing Buri").isStatus());
        Assertions.assertEquals(false, provinceService.validateProvince("Sing Buri!").isStatus());
        Assertions.assertEquals(false, provinceService.validateProvince("Sing Buri0").isStatus());
        Assertions.assertEquals(false, provinceService.validateProvince("Sing0 Buri!").isStatus());
        Assertions.assertEquals(false, provinceService.validateProvince("Sing Bu").isStatus());
        Assertions.assertEquals(false, provinceService.validateProvince("Sing Buรี").isStatus());
        Assertions.assertEquals(false, provinceService.validateProvince("SingBuri").isStatus());
//        Assertions.assertEquals(true, validateService.validateMobileNumber("0612345678").isStatus());
//        Assertions.assertEquals(false, validateService.validateMobileNumber("0212345678").isStatus());

//        Assertions.assertEquals(false, validateService.validatePhoneNumber("021245679").isStatus());
//        Assertions.assertEquals(false, validateService.validatePhoneNumber("0612245678").isStatus());

        //phone
//        Assertions.assertEquals(true, validateService.validatePhoneAndMobileNumber("0612345678").isStatus());
//        Assertions.assertEquals(true, validateService.validatePhoneAndMobileNumber("0833692540").isStatus());
//        Assertions.assertEquals(true, validateService.validatePhoneAndMobileNumber("0933692540").isStatus());
//        Assertions.assertEquals(true, validateService.validatePhoneAndMobileNumber("0433692540").isStatus());
//        Assertions.assertEquals(true, validateService.validatePhoneAndMobileNumber("023369254").isStatus());
//
//        Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("09336925").isStatus());
//        Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("09336925401").isStatus());
//        Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("1933692540").isStatus());
//        Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("193369254").isStatus());
//        Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("093369254!").isStatus());
//        Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("0933g925!").isStatus());
//        Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("0433g9250").isStatus());
//        Assertions.assertEquals(false, validateService.validatePhoneAndMobileNumber("0212 45678").isStatus());
    }
}